//
//  MovieDetailVideoTableViewCell.swift
//  Movies
//
//  Created by Nelson Bolivar on 3/4/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit
import YouTubePlayer

class MovieDetailVideoTableViewCell: UITableViewCell {

    @IBOutlet weak var videoTitleLabel: UILabel?
    @IBOutlet weak var videoPlayerView: UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(video: Video) {
        if let name = video.name {
            videoTitleLabel?.text = name
        }
        
        if let videoKey = video.videoKey {
            guard let bounds = videoPlayerView?.bounds else { return }
            let videoPlayer = YouTubePlayerView(frame: bounds)
            videoPlayer.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            videoPlayer.loadVideoID(videoKey)
            videoPlayerView?.addSubview(videoPlayer)
        }
    }
}
