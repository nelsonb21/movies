//
//  MoviesDetailDataSource.swift
//  Movies
//
//  Created by Nelson Bolivar on 3/4/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias MoviesDetailDataSource = MoviesDetailViewController

extension MoviesDetailDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1+videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MovieDetailInformationTableViewCell.self), for: indexPath) as? MovieDetailInformationTableViewCell else { return UITableViewCell() }
            guard let movie = movie else { return UITableViewCell() }
            cell.setupCell(movie: movie)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MovieDetailVideoTableViewCell.self), for: indexPath) as? MovieDetailVideoTableViewCell else { return UITableViewCell() }
            let video = videos[indexPath.row-1]
            cell.setupCell(video: video)
            return cell
        }
    }

}
