//
//  MoviesDataSource.swift
//  Movies
//
//  Created by Nelson Bolivar on 3/4/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

typealias MoviesDataSource = MoviesViewController

extension MoviesDataSource: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MoviesCollectionViewCell.self), for: indexPath) as? MoviesCollectionViewCell else { return UICollectionViewCell() }
        let movie = movies[indexPath.row]
        cell.setupCell(movie: movie)
        return cell
    }
    
}
