//
//  Color.swift
//  Movies
//
//  Created by Nelson Bolivar on 3/4/17.
//  Copyright © 2017 Nelson Bolivar. All rights reserved.
//

import UIKit

struct Color {
    static let selectedCategory = #colorLiteral(red: 0.8078431373, green: 0.1529411765, blue: 0.1607843137, alpha: 1)
    
}
