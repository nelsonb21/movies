import Foundation
import Async

class CartSync {
    
    var savings: Float = 0.0
    var products: [MProduct] = []
    var exclusivePrice: Float = 0.0
    var serviceFee: Float?
    var productsNotInDBids: Set<String>?
    
    func updateWithCart(cart: APIResponse, callback: ((_ status: ServiceStatus) -> Void) ) {
        savings = cart.saving
        exclusivePrice = cart.exclusivePrice
        serviceFee = cart.serviceFee
        let ids = cart.getProductIds()
        
        ProductsByUUIDQuery(productIds: ids).mutexExecute { (response: ProductsByIdQueryResponse?) in
            guard var dbProducts = response else {
                Async.main {
                    callback(status: .FailValidation)
                    log.error("Cant get products from DB with UUID")
                }
                return
            }
            let dbIds = Set(dbProducts.map({ $0.uuid }))
            self.productsNotInDBids = Set(ids).subtract(dbIds)
            dbProducts.forEach { product in
                product.appOrigin = .Sync
                cart.products.forEach { updateProduct in
                    guard updateProduct.upc == product.uuid else { return }
                    
                    if product.numOfProducts > 0 {
                        let newProduct = product.deepCopy("CopyProduct").then {
                            $0.updateProductSync(updateProduct)
                            $0.totalGroup = updateProduct.price / 100
                            product.numOfProducts += 1
                            $0.quantity = updateProduct.quantity
                        }
                        dbProducts.append(newProduct)
                    } else {
                        product.updateProductSync(updateProduct)
                        product.totalGroup = updateProduct.price / 100
                        product.numOfProducts += 1
                        product.quantity = updateProduct.quantity
                    }
                }
            }
            self.products = dbProducts
            Async.main {
                callback(status: .Ok)
            }
        }
    }
    
    func getProductIds() -> [String] {
        return products.flatMap { $0.identification }
    }
    
}
